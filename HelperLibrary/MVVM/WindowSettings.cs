﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace HelperLibrary.MVVM {
    public class WindowSettings : PropertyChangedBase {
        private bool _isOpen = false;

        public WindowStartupLocation StartupLocation { get; set; } = WindowStartupLocation.Manual;
        public object DataContext { get; set; }
        public WindowState State { get; set; } = WindowState.Normal;
        public bool IsOpen {
            get {
                return _isOpen;
            }
            set {
                _isOpen = value;
                OnPropertyChanged("IsOpen");
            }
        }

        public bool IsItDialog { get; set; } = false;
    }
}
