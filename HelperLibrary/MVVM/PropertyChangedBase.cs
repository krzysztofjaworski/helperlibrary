﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using HelperLibrary.Utils;

namespace HelperLibrary.MVVM {
    [Serializable]
    public class PropertyChangedBase : INotifyPropertyChanged {
        [field: NonSerialized]
        public virtual event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged([CallerMemberName] string property = "") {
            try {
                PropertyChangedEventHandler propertyChanged = PropertyChanged;
                propertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
            } catch (NullReferenceException e) {
                DebugHelper.DebugWriteLine(e.ToString());
            }
        }
        
        public virtual void NotifyPropertyChanges() {
            OnPropertyChanged($"");
        }
    }
}
