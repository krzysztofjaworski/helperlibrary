﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Threading;

namespace HelperLibrary.MVVM {

    [Serializable]
    public class AsyncObservableCollection<T> : ObservableCollection<T> {
        [NonSerialized]
        private SynchronizationContext _synchronizationContext = SynchronizationContext.Current;

        public AsyncObservableCollection() { }

        public AsyncObservableCollection(IEnumerable<T> list)
            : base(list) { }

        public AsyncObservableCollection(SynchronizationContext context) => _synchronizationContext = context;
        public AsyncObservableCollection(IEnumerable<T> list, SynchronizationContext context) : base(list) => _synchronizationContext = context;

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e) {
            if (_synchronizationContext == null) _synchronizationContext = SynchronizationContext.Current;

            if (SynchronizationContext.Current == _synchronizationContext) {
                RaiseCollectionChanged(e);
            } else {
                _synchronizationContext.Send(RaiseCollectionChanged, e);
            }
        }

        private void RaiseCollectionChanged(object param) {
            try {
                base.OnCollectionChanged((NotifyCollectionChangedEventArgs)param);

            } catch {

            }
        }

        protected override void OnPropertyChanged(PropertyChangedEventArgs e) {
            if (_synchronizationContext == null) _synchronizationContext = SynchronizationContext.Current;

            if (SynchronizationContext.Current == _synchronizationContext) {
                RaisePropertyChanged(e);
            } else {
                _synchronizationContext.Send(RaisePropertyChanged, e);
            }
        }

        private void RaisePropertyChanged(object param) => base.OnPropertyChanged((PropertyChangedEventArgs)param);

    }
}