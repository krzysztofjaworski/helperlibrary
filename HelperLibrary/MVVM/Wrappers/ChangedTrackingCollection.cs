﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace HelperLibrary.MVVM.Wrappers {
    public class ChangedTrackingCollection<T> : AsyncObservableCollection<T>, IValidatableTrackingObject
        where T : class, IValidatableTrackingObject {
        private IList<T> _originalCollection;

        private readonly AsyncObservableCollection<T> _addItems = new AsyncObservableCollection<T>();
        private readonly AsyncObservableCollection<T> _removedItems = new AsyncObservableCollection<T>();
        private readonly AsyncObservableCollection<T> _modifiedItems = new AsyncObservableCollection<T>();

        public ReadOnlyObservableCollection<T> AddedItems { get; }
        public ReadOnlyObservableCollection<T> RemovedItems { get; }
        public ReadOnlyObservableCollection<T> ModifiedItems { get; }

        public bool IsValid {
            get { return this.All(x => x.IsValid); }
        }

        public ChangedTrackingCollection() {
            _originalCollection = new List<T>();

            AttachItemPropertyChangedHandler(_originalCollection);

            AddedItems = new ReadOnlyObservableCollection<T>(_addItems);
            RemovedItems = new ReadOnlyObservableCollection<T>(_removedItems);
            ModifiedItems = new ReadOnlyObservableCollection<T>(_modifiedItems);
        }


        public ChangedTrackingCollection(IEnumerable<T> originalCollection) : base(originalCollection) {
            _originalCollection = originalCollection.ToList();

            AttachItemPropertyChangedHandler(_originalCollection);

            AddedItems = new ReadOnlyObservableCollection<T>(_addItems);
            RemovedItems = new ReadOnlyObservableCollection<T>(_removedItems);
            ModifiedItems = new ReadOnlyObservableCollection<T>(_modifiedItems);
        }

        private void AttachItemPropertyChangedHandler(IEnumerable<T> items) {
            foreach (var item in items) {
                item.PropertyChanged += ItemPropertyChanged;
            }
        }

        private void DetachItemPropertyChangedHandler(IEnumerable<T> items) {
            foreach (var item in items) {
                item.PropertyChanged -= ItemPropertyChanged;
            }
        }

        private void ItemPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == nameof(IsValid)) {
                OnPropertyChanged(new PropertyChangedEventArgs(nameof(IsValid)));
            }
            else {
                var item = (T) sender;
                if (_addItems.Contains(item)) return;
                if (item.IsChanged) {
                    if (!_modifiedItems.Contains(item)) _modifiedItems.Add(item);
                }
                else {
                    if (_modifiedItems.Contains(item)) {
                        _modifiedItems.Remove(item);
                    }
                }

                OnPropertyChanged(new PropertyChangedEventArgs(nameof(IsChanged)));
                OnPropertyChanged(new PropertyChangedEventArgs(nameof(IsValid)));
            }
        }


        public void AcceptChanges() {
            _addItems.Clear();
            _removedItems.Clear();
            _modifiedItems.Clear();
            foreach (T item in this) {
                item.AcceptChanges();
            }

            _originalCollection = this.ToList();
            OnPropertyChanged(new PropertyChangedEventArgs(nameof(IsChanged)));
        }

        public bool IsChanged => AddedItems.Count > 0 || RemovedItems.Count > 0 || ModifiedItems.Count > 0;

        public void RejectChanges() {
            foreach (T addedItem in _addItems) {
                Remove(addedItem);
            }

            foreach (var removedItem in _removedItems) {
                Add(removedItem);
            }

            foreach (T item in _modifiedItems) {
                item.RejectChanges();
            }

            OnPropertyChanged(new PropertyChangedEventArgs(nameof(IsChanged)));
        }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e) {
            void UpdateObservableCollection(AsyncObservableCollection<T> collection, IEnumerable<T> items) {
                collection.Clear();
                foreach (T item in items) {
                    collection.Add(item);
                }
            }

            var added = this.Where(current => _originalCollection.All(orig => orig != current)).ToArray();
            var removed = _originalCollection.Where(orig => this.All(current => current != orig)).ToArray();
            var modified = this.Except(added).Except(removed).Where(item => item.IsChanged).ToArray();

            AttachItemPropertyChangedHandler(added);
            DetachItemPropertyChangedHandler(removed);

            UpdateObservableCollection(_addItems, added);
            UpdateObservableCollection(_removedItems, removed);
            UpdateObservableCollection(_modifiedItems, modified);

            base.OnCollectionChanged(e);

            OnPropertyChanged(new PropertyChangedEventArgs(nameof(IsChanged)));
        }
    }
}