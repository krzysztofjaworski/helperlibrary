﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace HelperLibrary.MVVM.Wrappers {
    public class NotifyDataErrorInfoBase : PropertyChangedBase, INotifyDataErrorInfo {

        protected readonly IDictionary<string, List<string>> Errors = new Dictionary<string, List<string>>(); 

        public IEnumerable GetErrors(string propertyName) {
            return propertyName != null && Errors.ContainsKey(propertyName)
                ? Errors[propertyName]
                : Enumerable.Empty<string>();
        }

        protected virtual void OnErrorsChanged(string propertyName) {
            ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        }

        public bool HasErrors => Errors.Any();

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        
        protected void ClearErrors() {
            foreach (string propertyName in Errors.Keys.ToList()) {
                Errors.Remove(propertyName);
                OnErrorsChanged(propertyName);
            }
        }
    }
}
