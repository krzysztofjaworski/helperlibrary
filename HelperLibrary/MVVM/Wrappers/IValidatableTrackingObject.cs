using System.ComponentModel;

namespace HelperLibrary.MVVM.Wrappers {
    public interface IValidatableTrackingObject : IRevertibleChangeTracking, INotifyPropertyChanged {
        bool IsValid { get; }
    }
}