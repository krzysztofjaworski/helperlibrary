/*    I wrote this with help of course from plurasight.
 *    Adjusted also to work with NHibernate because
 *    this framework grab data from database in reccurenction mode.
 *    I changed collection to lazy loading.
 */

namespace HelperLibrary.MVVM.Wrappers {
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Class for wrapping model class. Add to class NotificationProperty, Validation nad Tracking model.
    /// </summary>
    /// <typeparam name="T">Model to wrap</typeparam>
    public abstract class BaseWrapper<T> : NotifyDataErrorInfoBase, IValidatableTrackingObject, IValidatableObject {

        private readonly IDictionary<string, object> _originalValues = new Dictionary<string, object>();
        private readonly IList<IValidatableTrackingObject> _trackingObjects = new List<IValidatableTrackingObject>();

        public T Model { get; }

        public bool IsChanged => _originalValues.Count > 0 || _trackingObjects.Any(x => x.IsChanged);
        public bool IsValid => !HasErrors && _trackingObjects.All(x => x.IsValid);

        public void AcceptChanges() {
            _originalValues.Clear();
            foreach (IValidatableTrackingObject tracking in _trackingObjects) {
                tracking.AcceptChanges();
            }
            OnPropertyChanged($"");
        }

        public void RejectChanges() {
            foreach (var originalValue in _originalValues) {
                typeof(T).GetProperty(originalValue.Key)?.SetValue(Model, originalValue.Value);
            }

            foreach (IValidatableTrackingObject tracking in _trackingObjects) {
                tracking.RejectChanges();
            }
            _originalValues.Clear();
            OnPropertyChanged($"");

        }

        public BaseWrapper(T model) {
            Model = model;
            // ReSharper disable once VirtualMemberCallInConstructor
            InitializeComplexProperties(Model);
            // ReSharper disable once VirtualMemberCallInConstructor
            InitializeCollectionsProperties(Model);
            // ReSharper disable once VirtualMemberCallInConstructor
            InitializeSimpleCollectionProperties(Model);
            Validate();
        }

        public BaseWrapper() {
            Model = Activator.CreateInstance<T>();
            // ReSharper disable once VirtualMemberCallInConstructor
            InitializeComplexProperties(Model);
            // ReSharper disable once VirtualMemberCallInConstructor
            InitializeCollectionsProperties(Model);
            // ReSharper disable once VirtualMemberCallInConstructor
            InitializeSimpleCollectionProperties(Model);
            Validate();
        }

        protected virtual void InitializeComplexProperties(T model) {

        }

        protected virtual void InitializeCollectionsProperties(T model) {

        }

        protected virtual void InitializeSimpleCollectionProperties(T model) {

        }

        protected ChangedTrackingCollection<TWrapperType> InitializeLazyCollection<TModelType, TWrapperType>(IList<TModelType> collection) where TWrapperType : BaseWrapper<TModelType> {
            ChangedTrackingCollection<TWrapperType> _asyncObservableCollection;
            if (collection != null)
                _asyncObservableCollection = new ChangedTrackingCollection<TWrapperType>(
                    collection.Select(x =>
                        (TWrapperType)Activator.CreateInstance(typeof(TWrapperType), x)));
            else {
                _asyncObservableCollection = new ChangedTrackingCollection<TWrapperType>();
            }

            RegisterCollection(_asyncObservableCollection, collection);
            return _asyncObservableCollection;
        }

        protected Lazy<ChangedTrackingCollection<TWrapper>> InitCollection<TModel, TWrapper>(IList<TModel> collection) where TWrapper : BaseWrapper<TModel> {
            return new Lazy<ChangedTrackingCollection<TWrapper>>(() => InitializeLazyCollection<TModel, TWrapper>(collection));
        }

        protected void RegisterSimpleCollection<TType>(string propertyName) {
            INotifyCollectionChanged _wrapperCollectionChanged = (INotifyCollectionChanged)GetType().GetProperty(propertyName)?.GetValue(this);
            if (_wrapperCollectionChanged == null) throw new NullReferenceException("Property should be type of INotifyCollectionChanged");

            IList<TType> _modelCollection =
                (IList<TType>)Model.GetType().GetProperty(propertyName)?.GetValue(Model);

            if (_modelCollection == null) throw new NullReferenceException("Model collection is null.");

            _wrapperCollectionChanged.CollectionChanged += (sender, args) => {
                switch (args.Action) {
                    case NotifyCollectionChangedAction.Add: {
                            foreach (var item in args.NewItems.Cast<TType>()) {
                                _modelCollection.Add(item);
                            }

                            break;
                        }
                    case NotifyCollectionChangedAction.Remove: {
                            foreach (var item in args.OldItems.Cast<TType>()) {
                                _modelCollection.Remove(item);
                            }

                            break;
                        }
                    case NotifyCollectionChangedAction.Reset:
                        _modelCollection.Clear();
                        break;
                }
            };
        }


        protected void RegisterCollection<TWrapper, TModel>(ChangedTrackingCollection<TWrapper> wrapperCollection,
            IList<TModel> modelCollection) where TWrapper : BaseWrapper<TModel> {

            wrapperCollection.CollectionChanged += (sender, args) => {
                if (args.OldItems != null) {
                    foreach (var wrapper in args.OldItems.Cast<TWrapper>()) {
                        modelCollection.Remove(wrapper.Model);
                    }
                }

                if (args.NewItems != null) {
                    foreach (var wrapper in args.NewItems.Cast<TWrapper>()) {
                        modelCollection.Add(wrapper.Model);
                    }
                }

                if (args.Action == NotifyCollectionChangedAction.Reset) {
                    modelCollection.Clear();
                }

                Validate();
            };

            RegisterTrackingObject(wrapperCollection);
        }

        protected void SetValueComplexProperty<TValue, TModel>(TValue value, TValue oldValue, [CallerMemberName] string propertyName = null) where TValue : BaseWrapper<TModel> {
            if (Equals(value, oldValue)) return;
            UntrackObject(value);
            RegisterComplexProperty(value);
            TModel currentValue = (TModel)Model?.GetType().GetProperty(propertyName ?? throw new ArgumentNullException(nameof(propertyName)))?.GetValue(Model);
            TModel newValue = (TModel)value?.GetType().GetProperty("Model")?.GetValue(value);
            if (Equals(currentValue, newValue)) return;
            PropertyInfo _propertyInfo = Model?.GetType().GetProperty(propertyName ?? throw new ArgumentNullException(nameof(propertyName)));
            _propertyInfo?.SetValue(Model, newValue);
        }

        protected void RegisterComplexProperty<TModel>(BaseWrapper<TModel> wrapper) {
            RegisterTrackingObject(wrapper);
        }

        private void RegisterTrackingObject(IValidatableTrackingObject trackingObject) {
            if (!_trackingObjects.Contains(trackingObject)) {
                TrackObject(trackingObject);
            }
        }

        private void TrackObject(IValidatableTrackingObject trackingObject) {
            _trackingObjects.Add(trackingObject);
            trackingObject.PropertyChanged += TrackingObjectOnPropertyChanged();
        }

        private PropertyChangedEventHandler TrackingObjectOnPropertyChanged() {
            return (sender, args) => {
                if (args.PropertyName == nameof(IsChanged)) {
                    OnPropertyChanged(nameof(IsChanged));
                } else if (args.PropertyName == nameof(IsValid)) {
                    OnPropertyChanged(nameof(IsValid));
                }
            };
        }

        protected void UntrackObject(IValidatableTrackingObject trackingObject) {
            if (trackingObject == null) return;
            _trackingObjects.Remove(trackingObject);
            trackingObject.PropertyChanged -= TrackingObjectOnPropertyChanged();
        }

        protected TValue GetValue<TValue>([CallerMemberName] string propertyName = null) {
            PropertyInfo _propertyInfo = Model.GetType().GetProperty(propertyName ?? throw new ArgumentNullException(nameof(propertyName)));
            Debug.Assert(_propertyInfo != null, nameof(_propertyInfo) + " != null");
            return (TValue)_propertyInfo.GetValue(Model);
        }

        protected TValue GetOriginalValue<TValue>(string propertyName) {
            return _originalValues.ContainsKey(propertyName)
                ? (TValue)_originalValues[propertyName]
                : GetValue<TValue>(propertyName);
        }

        protected bool GetIsChanged(string propertyName) {
            return _originalValues.ContainsKey(propertyName);
        }

        protected void SetValue<TValue>(TValue newValue, [CallerMemberName] string propertyName = null) {
            PropertyInfo _propertyInfo = Model.GetType().GetProperty(propertyName ?? throw new ArgumentNullException(nameof(propertyName)));
            if (_propertyInfo == null) return;
            var currentValue = _propertyInfo.GetValue(Model);
            if (!Equals(currentValue, newValue)) {
                UpdateOriginalValue(propertyName, currentValue, newValue);
                _propertyInfo.SetValue(Model, newValue);
                Validate();
                OnPropertyChanged(propertyName);
                OnPropertyChanged(propertyName + "IsChanged");
            }
        }

        private void Validate() {
            ClearErrors();
            var result = new List<ValidationResult>();
            var context = new ValidationContext(this);
            Validator.TryValidateObject(this, context, result, true);
            if (result.Any()) {
                var propertyNames = result.SelectMany(x => x.MemberNames).Distinct().ToList();
                foreach (string propertyName in propertyNames) {
                    Errors[propertyName] = result
                        .Where(x => x.MemberNames.Contains(propertyName))
                        .Select(x => x.ErrorMessage).Distinct().ToList();
                    OnErrorsChanged(propertyName);
                }
                OnPropertyChanged(nameof(IsValid));
            }
            OnErrorsChanged(nameof(IsValid));
        }

        private void UpdateOriginalValue<TValue>(string propertyName, TValue currentValue, TValue newValue) {
            if (!_originalValues.ContainsKey(propertyName)) {
                _originalValues.Add(propertyName, currentValue);
                OnPropertyChanged($"IsChanged");
            } else {
                if (Equals(_originalValues[propertyName], newValue)) {
                    _originalValues.Remove(propertyName);
                    OnPropertyChanged($"IsChanged");
                }
            }
        }
        
        /// <summary>
        /// Override because Validate method also invoke OnPropertyChanged for each property and validate property.
        /// No need to invoke OnPropertyChanged twice.
        /// </summary>
        public override void NotifyPropertyChanges() {
            Validate();
        }

        public Func<object, T, IEnumerable<ValidationResult>> ExternalValidation;

        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext) {
            yield break;
        }
    }
}