﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace HelperLibrary.MVVM.AttachedPropertys {
    public class EventToCommand : DependencyObject {
        public static string GetEventName(DependencyObject obj) {
            return (string)obj.GetValue(EventNameProperty);
        }

        public static void SetEventName(DependencyObject obj, string value) {
            obj.SetValue(EventNameProperty, value);
        }

        public static readonly DependencyProperty EventNameProperty =
            DependencyProperty.RegisterAttached("EventName", typeof(string), typeof(EventToCommand), new PropertyMetadata(string.Empty));



        public static ICommand GetCommand(DependencyObject obj) {
            return (ICommand)obj.GetValue(CommandProperty);
        }

        public static void SetCommand(DependencyObject obj, ICommand value) {
            obj.SetValue(CommandProperty, value);
        }

        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.RegisterAttached("Command", typeof(ICommand), typeof(EventToCommand), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(CommandChanged)));

        private static void CommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            string _eventName = GetEventName(d);

            if (string.IsNullOrEmpty(_eventName)) throw new ArgumentException(nameof(EventNameProperty));

            EventInfo _eventInfo = d.GetType().GetEvent(_eventName);
            if (_eventInfo == null) throw new Exception($"Event {_eventInfo} not found");
            Type t = _eventInfo.EventHandlerType;
            _eventInfo.AddEventHandler(d, new Action(() => {
                (e.NewValue as ICommand)?.Execute(null);
            }));

        }
    }
}
