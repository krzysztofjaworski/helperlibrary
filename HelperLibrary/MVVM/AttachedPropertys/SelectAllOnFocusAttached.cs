﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace HelperLibrary.MVVM.AttachedPropertys {
    public class SelectAllOnFocusAttached : DependencyObject {

        public static bool GetSelectAllOnFocus(DependencyObject obj) {
            return (bool)obj.GetValue(SelectAllOnFocusProperty);
        }

        public static void SetSelectAllOnFocus(DependencyObject obj, bool value) {
            obj.SetValue(SelectAllOnFocusProperty, value);
        }

        public static readonly DependencyProperty SelectAllOnFocusProperty =
            DependencyProperty.RegisterAttached("SelectAllOnFocus", typeof(bool), typeof(SelectAllOnFocusAttached), new FrameworkPropertyMetadata(false, PropertyChanged));

        private static void PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            if (d is TextBox _textBox) {
                if ((bool) e.NewValue)
                    _textBox.GotFocus += TextBoxOnGotFocus;
                else
                    _textBox.GotFocus -= TextBoxOnGotFocus;
            }
        }

        private static void TextBoxOnGotFocus(object sender, RoutedEventArgs e) {
            (sender as TextBox)?.SelectAll();
        }
    }
}
