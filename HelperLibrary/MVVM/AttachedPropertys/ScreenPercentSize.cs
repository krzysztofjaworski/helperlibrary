﻿using System.Windows;
using System.Windows.Forms;

namespace HelperLibrary.MVVM.AttachedPropertys {
    public class ScreenPercentSize : DependencyObject {
        private static readonly double Width;
        private static readonly double Height;

        static ScreenPercentSize() {
            Width = Screen.PrimaryScreen.Bounds.Width;
            Height = Screen.PrimaryScreen.Bounds.Height;
        }

        public static readonly DependencyProperty ScreenWidthProperty = DependencyProperty.RegisterAttached(
            "ScreenPercentSize", typeof(double), typeof(ScreenPercentSize),
            new PropertyMetadata(default(double), ChangedWidthProperty));

        private static void ChangedWidthProperty(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            if (d is FrameworkElement element) {
                element.Width = (Width / 100) * (double) e.NewValue;
            }
        }

        public static void SetScreenWidth(DependencyObject element, double value) {
            element.SetValue(ScreenWidthProperty, value);
        }

        public static double GetScreenWidth(DependencyObject element) {
            return (double) element.GetValue(ScreenWidthProperty);
        }


        public static readonly DependencyProperty ScreenHeightProperty = DependencyProperty.RegisterAttached(
            "ScreenHeight", typeof(double), typeof(ScreenPercentSize),
            new PropertyMetadata(default(double), ChangedHeightProperty));

        private static void ChangedHeightProperty(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            if (d is FrameworkElement element) {
                element.Height = (Height / 100) * (double) e.NewValue;
            }
        }

        public static void SetScreenHeight(DependencyObject element, double value) {
            element.SetValue(ScreenHeightProperty, value);
        }

        public static double GetScreenHeight(DependencyObject element) {
            return (double) element.GetValue(ScreenHeightProperty);
        }
    }
}