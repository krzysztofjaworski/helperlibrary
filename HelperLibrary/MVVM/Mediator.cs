using System;
using System.Collections.Generic;
using System.Linq;

namespace HelperLibrary.MVVM {
    
    /// <summary>
    /// Mediator pattern. Simplify communication in application.
    /// </summary>
    public static class Mediator {
        /// <summary>
        /// Throw exceptions when key is not found in <see cref="Notify"/> method.
        /// </summary>
        public static bool ThrowExceptionWhenKeyNotFoundOnNotify = true;
        /// <summary>
        /// Dictionary stores action to invoke.
        /// </summary>
        private static readonly IDictionary<string, List<Action<object>>> DictionaryActions = new Dictionary<string, List<Action<object>>>();
        
        /// <summary>
        /// Register new event and add action to it.
        /// </summary>
        /// <param name="key">Name of event.</param>
        /// <param name="action">Action to add.</param>
        public static void Register(string key, Action<object> action) {
            if (DictionaryActions.ContainsKey(key)) {
                if (DictionaryActions[key].All(x => x.Method.ToString() == action.ToString())) return;
                DictionaryActions[key].Add(action);
                return;
            }
            
            DictionaryActions.Add(key, new List<Action<object>>() {
                action
            });
        }
        
        /// <summary>
        /// Remove action from Mediator.
        /// </summary>
        /// <param name="key">Key of event.</param>
        /// <param name="action">Action to remove</param>
        public static void UnRegister(string key, Action<object> action) {
            if (DictionaryActions.ContainsKey(key)) {
                DictionaryActions[key].Remove(action);
            }
        }

        /// <summary>
        /// Notify registered events.
        /// </summary>
        /// <param name="key">Key of event.</param>
        /// <param name="args">Arguments for actions</param>
        /// <exception cref="KeyNotFoundException">Occurs when property <see cref="ThrowExceptionWhenKeyNotFoundOnNotify"/> is set to true.</exception>
        public static void Notify(string key, object args) {
            if (DictionaryActions.ContainsKey(key)) {
                foreach (Action<object> action in DictionaryActions[key]) {
                    action.Invoke(args);
                }
            }
            else if (ThrowExceptionWhenKeyNotFoundOnNotify) {
                throw new KeyNotFoundException($"Key {key} not registered.");
            }
        }
    }
}
