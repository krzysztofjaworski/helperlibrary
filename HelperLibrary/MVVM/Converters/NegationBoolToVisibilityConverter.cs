﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace HelperLibrary.MVVM.Converters {
    public class NegationBoolToVisibilityConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value is bool _value) return (!_value) ? Visibility.Visible : Visibility.Collapsed;

            throw new InvalidProgramException($"Value should be type of {typeof(bool)} not {value.GetType()}");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value is Visibility _value) return (_value != Visibility.Visible);

            throw new InvalidProgramException($"Value should be type of {typeof(Visibility)} not {value.GetType()}");
        }
    }
}
