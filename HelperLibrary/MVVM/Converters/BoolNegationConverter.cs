﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace HelperLibrary.MVVM.Converters
{
    public class BoolNegationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            return (value is bool b) ? (!b) : throw new InvalidCastException("Invalid value type");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value is bool b) ? (b) : throw new InvalidCastException("Invalid value type");
        }
    }
}
