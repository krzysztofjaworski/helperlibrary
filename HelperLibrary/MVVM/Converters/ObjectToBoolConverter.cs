﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace HelperLibrary.MVVM.Converters {
    public class ObjectToBoolConverter : IValueConverter {
        // Token: 0x06000049 RID: 73 RVA: 0x00002F74 File Offset: 0x00001174
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            bool flag = ((parameter != null) ? parameter.ToString() : null) == "!";
            object result;
            if (flag) {
                result = (value == null);
            } else {
                result = (value != null);
            }
            return result;
        }

        // Token: 0x0600004A RID: 74 RVA: 0x00002FBA File Offset: 0x000011BA
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
