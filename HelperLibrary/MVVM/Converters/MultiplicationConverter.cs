﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Decimal = System.Decimal;

namespace HelperLibrary.MVVM.Converters {
    public class MultiplicationConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value != null) {
                double _value = double.Parse(value.ToString());
                double _parameter = double.Parse(parameter.ToString());
                return _value * _parameter;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value != null) {
                double _value = double.Parse(value.ToString());
                double _parameter = double.Parse(parameter.ToString());
                return _value / _parameter;
            }

            return null;
        }
    }
}
