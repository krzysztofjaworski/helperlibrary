using System;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Data;

namespace HelperLibrary.MVVM.Converters {
    public class BoolToVisibilityConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value is bool _value) return (_value) ? Visibility.Visible : Visibility.Collapsed;
            
            throw new InvalidProgramException($"Value should be type of {typeof(bool)} not {value.GetType()}");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value is Visibility _value) return (_value == Visibility.Visible) ? true : false;
            
            throw new InvalidProgramException($"Value should be type of {typeof(Visibility)} not {value.GetType()}");
        }
    }
}