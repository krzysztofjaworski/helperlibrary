using System;
 using System.Globalization;
 using System.Windows.Data;
 
 namespace HelperLibrary.MVVM.Converters {
     public class EnumToStringConverter : IValueConverter {
         public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
             return value?.ToString();
         }
 
         public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
             return string.IsNullOrEmpty(value?.ToString()) ? null : Enum.Parse(targetType, value.ToString());
         }
     }
 }