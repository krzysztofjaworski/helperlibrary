﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace HelperLibrary.MVVM.Behaviors {
    public class DataGridSelectedItemsBlendBehavior : Behavior<DataGrid> {
        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItems", typeof(IEnumerable),
                typeof(DataGridSelectedItemsBlendBehavior),
                new FrameworkPropertyMetadata(null) {
                    BindsTwoWayByDefault = true
                });

        private void newValueINotifyCollectionChanged_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {

            if (e.Action == NotifyCollectionChangedAction.Add) {
                if (sender is IList list) {
                    foreach (object eNewItem in e.NewItems) {
                        list.Add(eNewItem);
                    }
                }
            }

            if (e.Action == NotifyCollectionChangedAction.Remove) {
                if (sender is IList list) {
                    foreach (object eOldItem in e.OldItems) {
                        list.Remove(eOldItem);
                    }
                }
            }
        }

        public IEnumerable SelectedItems {
            get {
                return (IEnumerable)GetValue(SelectedItemProperty);
            }
            set {
                SetValue(SelectedItemProperty, value);
            }
        }

        protected override void OnAttached() {
            base.OnAttached();
            this.AssociatedObject.SelectionChanged += OnSelectionChanged;
        }

        protected override void OnDetaching() {
            base.OnDetaching();
            if (this.AssociatedObject != null)
                this.AssociatedObject.SelectionChanged -= OnSelectionChanged;
        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (e.AddedItems != null && e.AddedItems.Count > 0 && this.SelectedItems != null) {
                foreach (object obj in e.AddedItems) {
                    if (SelectedItems is IList list) {
                        Type _type = list.GetType();
                        if (_type.GenericTypeArguments.Length > 0 && _type.GenericTypeArguments[0] == obj.GetType())
                            list.Add(obj);
                    }
                }
            }

            if (e.RemovedItems != null && e.RemovedItems.Count > 0 && this.SelectedItems != null) {
                foreach (object obj in e.RemovedItems)
                    if (SelectedItems is IList list) {
                        Type _type = list.GetType();
                        if (_type.GenericTypeArguments.Length > 0 && _type.GenericTypeArguments[0] == obj.GetType())
                            list.Remove(obj);
                    }
            }
        }
    }
}
