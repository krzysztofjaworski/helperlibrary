using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace HelperLibrary.MVVM.Behaviors {
    public class LockKeyboardFocusBehavior : Behavior<UIElement> {
        public static readonly DependencyProperty LockKeyboardFocusProperty =
            DependencyProperty.Register("LockKeyboardFocus", typeof(bool), typeof(TextBoxInputRegExBehaviour),
                new FrameworkPropertyMetadata(false));

        public bool LockKeyboardFocus {
            get { return (bool) GetValue(LockKeyboardFocusProperty); }
            set { SetValue(LockKeyboardFocusProperty, value); }
        }

        protected override void OnAttached() {
            AssociatedObject.PreviewLostKeyboardFocus += AssociatedObjectOnPreviewLostKeyboardFocus;
        }

        protected override void OnDetaching() {
            AssociatedObject.PreviewLostKeyboardFocus -= AssociatedObjectOnPreviewLostKeyboardFocus;
        }

        private void AssociatedObjectOnPreviewLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e) {
            if (LockKeyboardFocus) {
                e.Handled = true;
            }
        }
    }
}