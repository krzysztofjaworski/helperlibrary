﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace HelperLibrary.MVVM {
    public class WindowService<T, U> where T : Window where U : IWindowService,  new() {
        private T _window;
        public void ShowWindow(U dataContext) {
            _window = Activator.CreateInstance<T>();
            _window.DataContext = dataContext;
            _window.Show();
        }

        public void ShowDialog(U dataContext) {
            _window = Activator.CreateInstance<T>();
            _window.DataContext = dataContext;
            _window.ShowDialog();
        }

        public void CloseWindow() {
            if (_window == null) throw new NullReferenceException("Window was not showed. Please first show window.");
            _window.Close();
        }
    }
}
