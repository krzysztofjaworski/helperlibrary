﻿using System;
using System.Windows.Forms;

namespace HelperLibrary.MVVM {
    /// <summary>
    /// Helper interface for WindowBehavior to manage window or dialog.
    /// </summary>
    public interface IWindowService {
        /// <summary>
        /// Dialog result of dialog.
        /// </summary>
        bool DialogResult { get; set; }

        /// <summary>
        /// Method to close window or dialog.
        /// </summary>
        Action CloseMethod { get; set; }

        /// <summary>
        /// Event invoked when windows or dialog is closing.
        /// </summary>
        void CloseEvent();

        /// <summary>
        /// Occurs when window is initialized.
        /// </summary>
        void WindowInitialize();
    }
}