﻿using System;
using System.Windows.Forms;
using HelperLibrary.Utils;

namespace HelperLibrary.MVVM {
    public class ViewModelBase : PropertyChangedBase, IWindowService {
        public bool DialogResult { get; set; }
        
        public Action CloseMethod { get; set; }

        public virtual void CloseEvent() {
            DebugHelper.DebugWriteLine($"Closing ViewModel {GetType()}");
        }

        public virtual void WindowInitialize() {
            DebugHelper.DebugWriteLine($"Initialized window. {GetType()}");
        }

        public virtual void CloseWindow() {
            if (CloseMethod == null) throw new NullReferenceException();
            CloseMethod.Invoke();
        }
    }
}
