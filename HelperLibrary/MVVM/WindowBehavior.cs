﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interactivity;

namespace HelperLibrary.MVVM {
    public class WindowBehavior : Behavior<Window> {

        private Window _windowInstance;
        private Window _parent;

        public Type WindowType { get { return (Type)GetValue(WindowTypeProperty); } set { SetValue(WindowTypeProperty, value); } }
        public static readonly DependencyProperty WindowTypeProperty = DependencyProperty.Register("WindowType", typeof(Type), typeof(WindowBehavior), new PropertyMetadata(null));

        public bool Open { get { return (bool)GetValue(OpenProperty); } set { SetValue(OpenProperty, value); } }
        public static readonly DependencyProperty OpenProperty = DependencyProperty.Register("Open", typeof(bool), typeof(WindowBehavior), new FrameworkPropertyMetadata(false, OnOpenChanged) { BindsTwoWayByDefault = true });

        public WindowSettings Settings { get { return (WindowSettings)GetValue(SettingsProperty); } set { SetValue(SettingsProperty, value); } }
        public static readonly DependencyProperty SettingsProperty = DependencyProperty.Register("Settings", typeof(WindowSettings), typeof(WindowBehavior), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(SettingsChanged)));

        private static void SettingsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            if (!(e.NewValue is WindowSettings)) return;
            Binding binding = new Binding { Path = new PropertyPath("IsOpen"), Source = e.NewValue };
            BindingOperations.SetBinding(d, OpenProperty, binding);
        }

        protected override void OnAttached() {
            if (AssociatedObject is Window _window) {
                _parent = _window;
            }
        }

        private static void OnOpenChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            (d as WindowBehavior)?.OnOpenChanged(e);
        }

        private void OnOpenChanged(DependencyPropertyChangedEventArgs e) {
            WindowBehavior me = this;
            if ((bool)e.NewValue) {
                object instance = Activator.CreateInstance(me.WindowType);
                if (instance is Window window) {
                    if (Settings.DataContext is IWindowService _ser) {
                        _ser.WindowInitialize();
                    }
                    me._windowInstance = window;

                    if (me.Settings != null) {
                        if (window.IsLoaded)
                            window.Owner = _parent;
                        if (!window.IsLoaded && me.Settings != null &&
                            me.Settings.StartupLocation == WindowStartupLocation.CenterOwner)
                            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                        else
                            window.WindowStartupLocation = me.Settings.StartupLocation;

                        if (me.Settings.DataContext != null) {
                            window.DataContext = me.Settings.DataContext;
                            if (me.Settings.DataContext is IWindowService _service) {
                                _service.CloseMethod = () => {
                                    me._windowInstance?.Close();
                                };
                            }
                        }
                        window.WindowState = me.Settings.State;
                    }

                    window.Closing += (s, ev) => {
                        if (!me.Open) return;
                        me._windowInstance = null;
                        me.Open = false;

                        if (me.Settings.DataContext is IWindowService service) {
                            service?.CloseEvent();
                        }

                        if (me.Settings != null) me.Settings.IsOpen = false;
                    };
                    if (me.Settings == null || !me.Settings.IsItDialog) {
                        window.Show();
                    } else {
                        window.ShowDialog();
                    }
                } else {
                    throw new ArgumentException($"Type '{me.WindowType}' does not derive from System.Windows.Window.");
                }
            } else {
                me._windowInstance?.Close();
            }
        }
    }
}
