﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using HelperLibrary.Utils;

namespace HelperLibrary.Comparers {
    public class SemiNumericComparer : IComparer<string> {

        public int Compare(string x, string y) {
            string _string1 = string.Join("", x.Where(s => char.IsLetter(s)));
            string _string2 = string.Join("", y.Where(s => char.IsLetter(s)));

            if (string.Compare(_string1, _string2) == 0) {
                int _number1 = string.Join("",x.Where(d => char.IsDigit(d))).ToInt32();
                int _number2 = string.Join("",y.Where(d => char.IsDigit(d))).ToInt32();

                if (_number1 > _number2) return 1;
                if (_number1 < _number2) return -1;
                if (_number1 == _number2) return 0;
            }

            return string.Compare(_string1, _string2);
        }
    }
}
