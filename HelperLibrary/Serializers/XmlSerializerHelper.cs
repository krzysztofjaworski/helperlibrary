﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HelperLibrary.Serializers {
    /// <summary>
    /// XML Serializer helper.
    /// </summary>
    public static class XmlSerializerHelper<T> {
        /// <summary>
        /// Deserialize object with specific T type.
        /// </summary>
        /// <typeparam name="T">Type of objcet to deserialize.</typeparam>
        /// <param name="path">Path xml file with serialized object.</param>
        /// <param name="log">String field where will be passed log.</param>
        /// <returns>Deserialized object with specific type.</returns>
        /// <exception cref="FileNotFoundException">Xml file nott found.</exception>
        /// <exception cref="TypeAccessException">Throw when T is not seriable.</exception>
        public static T DeserializeData(string path, out string log) {

            if (!typeof(T).IsSerializable)
                throw new TypeAccessException($"{typeof(T).ToString()} is not serializable.");

            if (File.Exists(path)) {
                try {
                    XmlSerializer _deserializer = new XmlSerializer(typeof(T));
                    T _deserializedObject = default(T);
                    using (TextReader _reader = new StreamReader(path)) {
                        _deserializedObject = (T)Convert.ChangeType(_deserializer.Deserialize(_reader), typeof(T));
                    }
                    log = $"Deserialize sucessfully object with type {typeof(T).ToString()}.";
                    return _deserializedObject;
                } catch (Exception e) {
                    log = $"Unexpected error: {e.ToString()}";
                    throw;
                }
            } else {
                log = $"File not found int path {path}";
                throw new FileNotFoundException(log);
            }
        }

        /// <summary>
        /// Deserialize object with specific T type.
        /// </summary>
        /// <typeparam name="T">Type of objcet to deserialize.</typeparam>
        /// <param name="path">Path xml file with serialized object.</param>
        /// <returns>Deserialized object with specific type.</returns>
        /// <exception cref="FileNotFoundException">Xml file nott found.</exception>
        /// <exception cref="TypeAccessException">Throw when T is not seriable.</exception>
        public static T DeserializeData(string path) {
            if (!typeof(T).IsSerializable)
                throw new TypeAccessException($"{typeof(T).ToString()} is not serializable.");

            if (File.Exists(path)) {
                try {
                    XmlSerializer _deserializer = new XmlSerializer(typeof(T));
                    T _deserializedObject = default(T);
                    using (TextReader _reader = new StreamReader(path)) {
                        _deserializedObject = (T)Convert.ChangeType(_deserializer.Deserialize(_reader), typeof(T));
                    }
                    return _deserializedObject;
                } catch {
                    throw;
                }
            } else {
                throw new FileNotFoundException("File not found in path " + path);
            }
        }

        /// <summary>
        /// Serialize objcet market as Serializable
        /// </summary>
        /// <typeparam name="T">Type of serialized object</typeparam>
        /// <param name="path">Path where object should be serialized</param>
        /// <param name="obj">Object to serialize</param>
        /// <exception cref="TypeAccessException">Throw when T is not seriable.</exception>
        public static void SerializeData(string path, T obj) {
            if (!typeof(T).IsSerializable)
                throw new TypeAccessException($"{typeof(T).ToString()} is not serializable.");

            XmlSerializer _serializer = new XmlSerializer(typeof(T));
            try {
                using (TextWriter _writer = new StreamWriter(path)) {
                    _serializer.Serialize(_writer, obj);
                }
            } catch {
                throw;
            }
        }

        /// <summary>
        /// Serialize objcet market as Serializable
        /// </summary>
        /// <typeparam name="T">Type of serialized object</typeparam>
        /// <param name="path">Path where object should be serialized</param>
        /// <param name="obj">Object to serialize</param>
        /// <param name="log">Set log value</param>
        /// <returns>True if sucessfully, False for fail.</returns>
        /// <exception cref="TypeAccessException">Throw when T is not seriable.</exception>
        public static bool SerializeData(string path, T obj, out string log) {
            if (!typeof(T).IsSerializable) {
                log = "This data type is not marke as Serializable";
                return false;
            }


            XmlSerializer _serializer = new XmlSerializer(typeof(T));
            try {
                using (TextWriter _writer = new StreamWriter(path)) {
                    _serializer.Serialize(_writer, obj);
                }
                log = "Serialization successfully complate";
                return true;
            } catch (Exception e) {
                log = "Serialization error. " + e.ToString();
                return false;
            }
        }
    }
}
