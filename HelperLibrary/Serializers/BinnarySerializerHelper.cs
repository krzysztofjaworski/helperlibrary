﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace HelperLibrary.Serializers {
    public static class BinarySerializer<T> {
        /// <summary>
        /// Deserialize object with specific T type.
        /// </summary>
        /// <typeparam name="T">Type of objcet to deserialize.</typeparam>
        /// <param name="path">Path to file with serialized object.</param>
        /// <param name="log">String field where will be passed log.</param>
        /// <returns>Deserialized object with specific type.</returns>
        /// <exception cref="FileNotFoundException">Binary file not found.</exception>
        /// <exception cref="TypeAccessException">Throw when T is not seriable.</exception>
        public static T DeserializeData(string path, out string log) {

            if (!typeof(T).IsSerializable)
                throw new TypeAccessException($"{typeof(T).ToString()} is not serializable.");

            if (File.Exists(path)) {
                try {
                    BinaryFormatter _deserializer = new BinaryFormatter();
                    T _deserializedObject = default(T);
                    using (FileStream _reader = new FileStream(path, FileMode.Open)) {
                        _deserializedObject = (T)_deserializer.Deserialize(_reader);
                    }
                    log = $"Deserialize object with type {typeof(T).ToString()}.";
                    return _deserializedObject;
                } catch (Exception e) {
                    log = $"Unexpected error: {e.ToString()}";
                    throw;
                }
            } else {
                log = $"File not found int path {path}";
                throw new FileNotFoundException(log);
            }
        }

        /// <summary>
        /// Deserialize object with specific T type.
        /// </summary>
        /// <typeparam name="T">Type of objcet to deserialize.</typeparam>
        /// <param name="path">Path to file with serialized object.</param>
        /// <returns>Deserialized object with specific type.</returns>
        /// <exception cref="FileNotFoundException">Binary file not found.</exception>
        /// <exception cref="TypeAccessException">Throw when T is not seriable.</exception>
        public static T DeserializeData(string path) {

            if (!typeof(T).IsSerializable)
                throw new TypeAccessException($"{typeof(T).ToString()} is not serializable.");

            if (File.Exists(path)) {
                try {
                    BinaryFormatter _deserializer = new BinaryFormatter();
                    T _deserializedObject = default(T);
                    using (FileStream _reader = new FileStream(path, FileMode.Open)) {
                        _deserializedObject = (T)_deserializer.Deserialize(_reader);
                    }
                    return _deserializedObject;
                } catch {
                    throw;
                }
            } else {
                throw new FileNotFoundException("File not found in path " + path);
            }
        }

        /// <summary>
        /// Serialize objcet market as Serializable
        /// </summary>
        /// <typeparam name="T">Type of serialized object</typeparam>
        /// <param name="path">Path where object should be serialized</param>
        /// <param name="obj">Object to serialize</param>
        /// <exception cref="TypeAccessException">Throw when T is not seriable.</exception>
        public static void SerializeData(string path, T obj) {
            if (!typeof(T).IsSerializable)
                throw new TypeAccessException($"{typeof(T).ToString()} is not serializable.");

            if (obj != null || !string.IsNullOrEmpty(path)) {
                BinaryFormatter _serializer = new BinaryFormatter();
                try {
                    using (FileStream _writer = new FileStream(path, FileMode.Create)) {
                        _serializer.Serialize(_writer, obj);
                    }
                } catch {
                    throw;
                }
            }
        }

        /// <summary>
        /// Serialize objcet market as Serializable
        /// </summary>
        /// <typeparam name="T">Type of serialized object</typeparam>
        /// <param name="path">Path where object should be serialized</param>
        /// <param name="obj">Object to serialize</param>
        /// <param name="log">Set log value</param>
        /// <returns>True if sucessfully, False for fail.</returns>
        /// <exception cref="TypeAccessException">Throw when T is not seriable.</exception>
        public static bool SerializeData(string path, T obj, out string log) {
            if (!typeof(T).IsSerializable) {
                log = "This data type is not marke as Serializable";
                return false;
            }

            if (obj != null || !string.IsNullOrEmpty(path)) {
                BinaryFormatter _serializer = new BinaryFormatter();
                try {
                    using (FileStream _writer = new FileStream(path, FileMode.Create)) {
                        _serializer.Serialize(_writer, obj);
                    }
                    log = "Serialization successfully complate";
                    return true;
                } catch (Exception e) {
                    log = "Serialization error. " + e.ToString();
                    return false;
                }
            }
            log = "Object is null or path is wrong. Serialization operation failed.";
            return false;
        }
    }
}
