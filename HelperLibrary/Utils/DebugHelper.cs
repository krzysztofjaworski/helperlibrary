﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelperLibrary.Utils {
    public static class DebugHelper {

        public static void DebugWriteLine(string line) {
#if DEBUG
            Debug.WriteLine($"##### {line}");
#endif
        }
    }
}
