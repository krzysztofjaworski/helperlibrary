﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HelperLibrary.Utils {
    public static class TaskWait {
        public static Task Wait(int miliseconds) {
            return Task.Factory.StartNew(() => Thread.Sleep(miliseconds));
        }
    }
}
