﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelperLibrary.Utils {
    public static class Extensions {
        public static int ToInt32(this string s) {
            return int.Parse(s);
        }
    }
}
