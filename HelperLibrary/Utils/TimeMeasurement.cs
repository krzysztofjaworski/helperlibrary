﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelperLibrary.Utils {
    public class TimeMeasurement : IDisposable {
        private readonly Stopwatch _watcher;
        private readonly string _message = null;

        public TimeMeasurement() {
            _watcher = new Stopwatch();
            _watcher.Start();
        }

        public TimeMeasurement(string message) {
            _message = message;
            _watcher = new Stopwatch();
            _watcher.Start();
        }

        public void Dispose() {
            _watcher.Stop();
            Debug.WriteLine(!string.IsNullOrEmpty(_message)
                ? $"TM: {_message}: {_watcher.ElapsedMilliseconds} ms"
                : $"TM: Time measurement: {_watcher.ElapsedMilliseconds} ms");
        }
    }
}
