﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelperLibrary.Common {
    public interface ISubscriber<TEventType> {
        void OnEventHandler(TEventType e);
    }
}
