﻿namespace HelperLibrary.Common {
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class FileSearcher {
        public event EventHandler<FileSearcherEventArgs> FileAdd;

        void OnFileAdd(object sender, FileSearcherEventArgs e) {
            if (FileAdd != null) {
                FileAdd(sender, e);
            }
        }

        public List<string> Files { get; private set; }

        public FileSearcher() {
            Files = new List<string>();
        }

        private void GetFiles(string path, string mask = "") {
            try {
                if (mask != "") {
                    foreach (string file in Directory.GetFiles(path, mask)) {
                        Files.Add(file);
                        FileAdd(this, new FileSearcherEventArgs(file, FileSearchType.File));
                    }
                    foreach (string dir in Directory.GetDirectories(path)) {
                        Files.Add(dir);
                        FileAdd(this, new FileSearcherEventArgs(dir, FileSearchType.Directory));
                        GetFilesRecursive(dir, mask);
                    }
                } else {
                    foreach (string file in Directory.GetFiles(path)) {
                        Files.Add(file);
                        FileAdd(this, new FileSearcherEventArgs(file, FileSearchType.File));
                    }
                    foreach (string dir in Directory.GetDirectories(path)) {
                        Files.Add(dir);
                        FileAdd(this, new FileSearcherEventArgs(dir, FileSearchType.Directory));
                        GetFilesRecursive(dir, mask);
                    }
                }                
            } catch {
                throw;
            }
        }

        public void GetFilesRecursive(string path) {
            GetFiles(path);
        }

        public void GetFilesRecursive(string path, string mask) {
            GetFiles(path, mask);
        }
    }

    public class FileSearcherEventArgs : System.EventArgs {
        public string FileName { get; private set; }
        public string FilePath { get; private set; }
        public FileSearchType FileType { get; private set; }

        public FileSearcherEventArgs(string filePath, FileSearchType fileType) {
            this.FilePath = filePath;
            this.FileName = getName(filePath);
            this.FileType = fileType;
        }

        string getName(string filePath) {
            return filePath.Substring(filePath.LastIndexOf('\\') + 1, filePath.Length - filePath.LastIndexOf('\\') - 1);
        }
    }

    public enum FileSearchType {
        Directory,
        File
    }
}
