﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelperLibrary.Common.Args {
    public class NewOldEventArgs : EventArgs {
        public NewOldEventArgs(object oldValue, object newValue) {
            OldValue = oldValue;
            NewValue = newValue;
        }

        public object OldValue { get; private set; }
        public object NewValue { get; private set; }
    }
}
