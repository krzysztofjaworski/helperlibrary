using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using HelperLibrary.MVVM;
using HelperLibrary.Utils;
using System.Threading.Tasks;

namespace HelperLibrary.Extensions {
    public static class Extensions {


        public static IEnumerable<string> GetEnumValues<T>() where T : Enum {
            return Enum.GetNames(typeof(T));
        }
        
        
        public static int GetWeekOfYear(this DateTime time)
        {
            
            int result = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
            
            if (result == 53) {
                int diff = 7 - (int)time.DayOfWeek;
                if (!Equals(time.Year, time.AddDays(diff).Year)) {
                    return 1;
                }
                
            }

            return result;
        } 
        
        public static int ToInt32(this string value) {
            return Convert.ToInt32(value);
        }

        public static int ToInt32(this object value) {
            return Convert.ToInt32(value);
        }

        public static IEnumerable<T> ExceptElement<T>(this IEnumerable<T> collection, T element) {
            bool flag = collection == null;
            if (flag) {
                throw new ArgumentNullException("collection");
            }

            return collection.Except(new T[] {
                element
            });
        }

        public static IEnumerable<T> FindVisualChildren<T>(this DependencyObject depObj)
            where T : DependencyObject {
            if (depObj == null) yield break;
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++) {
                DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                if (child is T searchedElement) {
                    yield return searchedElement;
                }
                foreach (T childOfChild in FindVisualChildren<T>(child)) {
                    yield return childOfChild;
                }
            }
        }

        public static T FindVisualChild<T>(this DependencyObject obj)
            where T : DependencyObject {
            return FindVisualChildren<T>(obj).FirstOrDefault();
        }

        public static T OpenWindow<T>(this WindowSettings settings) where T : IWindowService {
            T viewmodel = Activator.CreateInstance<T>();
            settings.DataContext = viewmodel;
            settings.IsOpen = true;
            return viewmodel;
        }

        public static T OpenWindow<T>(this WindowSettings settings, params object[] parameter) where T : IWindowService {
            T viewmodel = (T) Activator.CreateInstance(typeof(T), parameter);
            settings.DataContext = viewmodel;
            settings.IsOpen = true;
            return viewmodel;
        }

        public static T GetDataContext<T>(this WindowSettings settings, params object[] parameter) where T : IWindowService {
            T viewmodel = (T)Activator.CreateInstance(typeof(T), parameter);
            settings.DataContext = viewmodel;
            return viewmodel;
        }
    }
}